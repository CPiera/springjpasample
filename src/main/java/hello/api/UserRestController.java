package hello.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hello.application.UserController;
import hello.application.dto.UserDTO;

@RestController
public class UserRestController {

	@Autowired
	private UserController controller;

	@RequestMapping("/register")
	public String registerUser(@RequestParam String email, @RequestParam String password, @RequestParam String name)
			throws Exception {
		UserDTO user = controller.register(email, password, name);
		return "Registered " + user.getName();
	}

	@RequestMapping("/login")
	public String loginUser(@RequestParam String email, @RequestParam String password) throws Exception {
		UserDTO user = controller.loginUser(email, password);
		return "Identificador: " + user.getIdUser();

	}

	@RequestMapping("/allUsers")
	public String getAllUsers() throws Exception {
		List<UserDTO> result = controller.getAllUsers();
		String text = "";
		for (UserDTO u : result) {
			text += "Name: " + u.getName() + " Id: " + u.getIdUser() + "Email: " + u.getEmail() + "<br/>" ;
		}
		return text;

	}

}
