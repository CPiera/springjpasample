package hello.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import hello.utilities.InvalidParamException;

@Entity(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE) // li diem que ell sol autoincrementi el valor
	private int id;
	private String name;
	private String email;
	// si volem renombrar uuna columna de la taula @Colum(name="pass")
	private String password;

	public User() {

	}

	public User(String email, String password, String name) {
		this.email = email;
		this.password = password;
		this.name = name;
	}

	public int getIdUser() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void checkPasswordCorrect(String password) throws InvalidParamException {
		if (!password.equals(password))
			throw new InvalidParamException();

	}

}
