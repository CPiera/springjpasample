package hello.persistance;

import org.springframework.data.repository.CrudRepository;

import hello.domain.User;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface HelperUserRepository extends CrudRepository<User, Integer> {
	public User findByEmail(String email);
}
