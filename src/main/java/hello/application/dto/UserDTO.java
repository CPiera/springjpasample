package hello.application.dto;

import hello.domain.User;
import hello.utilities.NotFoundException;

public class UserDTO {

	private int id;
	private String name;
	private String email;

	public UserDTO(User user) throws NotFoundException {
		if (user == null)
			throw new NotFoundException();
		this.name = user.getName();
		this.id = user.getIdUser();
		this.email = user.getEmail();
	}

	public int getIdUser() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}
}
