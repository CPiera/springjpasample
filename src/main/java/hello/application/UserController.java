package hello.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import hello.application.dto.UserDTO;
import hello.domain.User;
import hello.persistance.UserRepository;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Controller
public class UserController {
	@Autowired
	private UserRepository userRepository; // creem una variable

	public UserDTO register(String email, String password, String name)
			throws InvalidParamException, NotFoundException {
		User user = new User(email, password, name);

		// aqui comprovar si el nom és repetit... etc.
		userRepository.save(user);
		return new UserDTO(user);

	}

	public UserDTO loginUser(String email, String password) throws InvalidParamException, NotFoundException {
		User user = userRepository.getUserByEmail(email);
		user.checkPasswordCorrect(password);
		return new UserDTO(user);
	}

	public List<UserDTO> getAllUsers() throws NotFoundException {
		List<UserDTO> usersDTO = new ArrayList<>();
		List<User> users = userRepository.getAllUsers();
		for (User u : users) {
			usersDTO.add(new UserDTO(u));
		}
		return usersDTO;
	}

}
